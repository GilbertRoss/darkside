# DarkSide




## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
  * [My choices](#my-choices)
* [Getting Started](#getting-started)
  * [Functionalities](#prerequisites)
* [Acknowledgements](#acknowledgements)



## About The Project

![Product Name Screen Shot][product-screenshot]



### Built With
This app was build using: 

* [Gridsome](https://gridsome.org/)
* [Express.js](https://expressjs.com/)
* [Tailwind.css](https://tailwindcss.com/)
* [mongoDB](https://www.mongodb.com/)



### My choices

I decided to use gridsome for the fronted  because it is a excellent Vue.js framework to develop static websites. I used it because I was familiar with it. 

For the api requests I used axios that is a promise based HTTP Client for Nodejs, and makes it easy to send asynchronous HTTP requests to REST endpoints and perform CRUD operations. 

To store the statistics I used a mongoDB cluster. I used it mainly because it was easy to implement it and matched perfectly the needs of the application. 

I hosted the backend on Heroku and the frontend on Zeit Now. 




## Getting Started

A demo can be viewed on: [https://darkside-one.now.sh](https://darkside-one.now.sh) 
or
you could run a local copy on your computer.

API : https://data-tellers-dark-side.herokuapp.com/api/stats/

### To run locally

1. Open terminal, go to the server folder
~~~~
cd server
npm install
npm run dev
~~~~
2. open terminal, go to the client folder

~~~~
cd client
npm install
npm gridsome develop
~~~~

## Functionalities

By clicking the lightbulb at the center of the page you can change the theme of the application. It has a button called statistics where by clicking on it you could see some statistics about the switch. 

As for backend I used the free tier of heroku, after clicking the statistics button, wait a few minutes.



## Acknowledgements
* [Axios](https://github.com/axios/axios)
* [Cors](https://developer.mozilla.org/it/docs/Web/HTTP/CORS)
* [Heroku](https://heroku.com)
* [Zeit.co](https://zeit.co)





[product-screenshot]: https://i.postimg.cc/fTD5QJt7/Screenshot-2020-02-26-Dark-Side.png