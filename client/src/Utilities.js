import axios from 'axios';

const url = 'https://data-tellers-dark-side.herokuapp.com/api/stats/';

class Utilities {
    // Get stats
    static getStats() {
        return new Promise(async(resolve, reject) => {
            try {
                const res = await axios.get(url);
                const data = res.data;
                resolve(data.map(stat => ({
                    ...stat,
                    lastupdate: new Date(stat.lastupdate)
                })));
            } catch (err) {
                reject(err);
            }
        })
    };

    static getId() {
        return new Promise(async(resolve, reject) => {
            try {
                const res = await axios.get(url);
                const data = res.data;
                resolve(data[0]._id);
            } catch (er) {
                reject(er)
            }
        })
    };

    static getOverallCounter() {
        return new Promise(async(resolve, reject) => {
            try {
                const res = await axios.get(url);
                const data = res.data;
                resolve(data[0].overall_counter);
            } catch (er) {
                reject(er)
            }
        })
    };


    // get total elapsedtime 
    static getElapsedTime() {
        return new Promise(async(resolve, reject) => {
            try {
                const res = await axios.get(url);
                const data = res.data;
                resolve(data[0].time_elapsed);
            } catch (er) {
                reject(er)
            }
        })
    };

    // Create stats

    static createStat(active, overall_counter, time_elapsed) {
        return axios.post(url, {
            active,
            overall_counter,
            time_elapsed,

        });
    };


    //  Delete stats using an id 
    static deleteStat(id) {
        return axios.delete(url + id);
    }

    // Update stats using heroku api endpoint

    static updateStat(id, active, overall_counter, time_elapsed) {
        return axios.put(url + id, {
            active,
            overall_counter,
            time_elapsed,
        });
    }
}

export default Utilities;