require('~/main.css')


import DefaultLayout from '~/layouts/Default.vue';
import 'vue-material-design-icons/styles.css';


export default function(Vue, { router, head, isClient }) {
    // Set default layout as a global component
    Vue.component('Layout', DefaultLayout)
}