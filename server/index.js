const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');


const app = express();

//Middle ware

app.use(bodyParser.json());
app.use(cors());

const stats = require('./routes/api/stats')

app.use('/api/stats', stats);
const port = process.env.PORT || 5000;

app.listen(port, () => console.log("Server started on port " + port));