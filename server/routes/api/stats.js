const express = require('express');
const mongodb = require('mongodb');

const router = express.Router();

// Get stats

router.get('/', async(req, res) => {
    const stats = await loadStatsCollection();
    res.send(await stats.find({}).toArray());
});

// Add Stats

router.post('/', async(req, res) => {
    const stats = await loadStatsCollection();
    await stats.insertOne({
        active: req.body.active,
        overall_counter: req.body.overall_counter,
        time_elapsed: req.body.time_elapsed,
        lastupdate: new Date(),
    });

    res.status(201).send();
})

// Update Stats

router.put('/:id', async(req, res) => {
    const stats = await loadStatsCollection();
    await stats.updateOne({ _id: new mongodb.ObjectID(req.params.id) }, // Filter
        {
            $set: {
                active: req.body.active,
                overall_counter: req.body.overall_counter,
                time_elapsed: req.body.time_elapsed,


            },
            $currentDate: { lastupdate: true }
        }
    );

    res.status(200).send();
})


// Delete Stats
router.delete('/:id', async(req, res) => {
    const stats = await loadStatsCollection();
    await stats.deleteOne({ _id: new mongodb.ObjectID(req.params.id) })
    res.status(200).send();
})

async function loadStatsCollection() {
    const client = await mongodb.MongoClient.connect('mongodb+srv://userOne:50NcmpGQ9uAhsjd9@cluster0-rkfma.mongodb.net/test?retryWrites=true&w=majority', {
        useUnifiedTopology: true,
    });

    return client.db('cluster0').collection('stats');
}

module.exports = router;